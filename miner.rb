#!/usr/bin/env ruby
#coding: UTF-8

require 'tk'
require 'pp'

require File.absolute_path(".") + "/functions"


$pole = Array.new
$btns = Array.new
$btns_array = Array.new

def clickButton (i, k, recursive=0)
	index = i*10+k
	$btns[index].state = 'disable'
	$btns[index].text = $btns_array[index]

	nearCell(index).each { |i| clickButton(0, index+i, 1) if $btns[index+i].state != "disabled" } if $btns_array[index].nil?

	if $btns_array[index] == "B"
		Tk::messageBox :message => "Вы проиграли."
		exit
	end

	checkWin($btns) if recursive < 1
end

def checkWin (buttons)
	cnt = 0
	buttons.each { |b| cnt += 1 if b.state == "normal" }

	if cnt == 10
		puts "winnnnn"
		Tk::messageBox :message => "Игра завершена! Вы молодец!"
	end
end

$window = TkRoot.new do
	title "Сапёр-минёр"
	minsize(400, 400)
end

# drawing buttons
10.times { |i| 
	10.times { |k|
		$btns << TkButton.new($window) {
			command { clickButton(i, k) }
			grid('row'=>i, 'column'=>k)
			height 2
			width 3
		}
	}
}



# generating bombs
bombs = Array.new
until bombs.length > 10 do
	t = 0+rand(100)
	if ! bombs.include? t
		$btns_array[t-1] = "B"
		bombs << t-1
	end
end

# generating numbers
$btns.each_with_index do |b, index|
	cnt = 0
	nearCell(index).each  { |i| cnt += 1 if $btns_array[index+i] == "B" }
	$btns_array[index] = cnt if $btns_array[index] != "B" && cnt > 0
end

Tk.mainloop












