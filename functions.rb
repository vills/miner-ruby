def logging(msg)
	puts "Logging: #{msg}"
end

def nearCell (index)
	arr = [-11, -10, -9, -1, 1, 9, 10, 11]

	# govnocode solutions
	if index % 10 == 0 then # => left side buttons
		arr.delete(-11)
		arr.delete(-1)
		arr.delete(9)
	end
	if (index+1)%10 == 0 then # => right
		arr.delete(-9)
		arr.delete(1)
		arr.delete(11)
	end
	if (index-9)<1 then # => top
		arr.delete(-11)
		arr.delete(-10)
		arr.delete(-9)
	end
	if (index+10)>99 then # => bottom
		arr.delete(9)
		arr.delete(10)
		arr.delete(11)
	end

	return arr
end